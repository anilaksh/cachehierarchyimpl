/* 
 * Created on March 28, 2016, 5:16 PM
 */

#include <cstdlib>
#include <iostream>
#include <list>
#include <map>
#include <iterator>

#define MAX_CACHE_LEVEL 3
#define CACHE_MISS 0
#define CACHE_HIT 1
using namespace std;

//forward declaration
template <typename key_type, typename value_type> class CacheManager;

//class to handle each cache
template <typename key_type, typename value_type> class Cache {

private:
    typedef list<key_type> key_list_type;
    typedef typename key_list_type::iterator key_list_itr;
    typedef struct {
        value_type value;
        bool dirty_bit;
        key_list_itr it;
    } struct_value_type;
    typedef map<key_type, struct_value_type> cache_hash_type;
    typedef typename cache_hash_type::iterator cache_hash_itr;
    key_list_type key_list;
    cache_hash_type cache_hash;
    
    int capacity;
    friend class CacheManager<key_type, value_type>;
public:

    Cache() {
    }

    void setCapacity(int capacity) {
        this->capacity = capacity;
    }

    int fetch(const key_type& k) {
        cache_hash_itr it = cache_hash.find(k);
        if(it == cache_hash.end()) {
            return CACHE_MISS;
        } else {
            return CACHE_HIT;
        }
    }
    
    bool isFull(void) {
        return (key_list.size() == capacity);
    }

    void splice(cache_hash_itr it) {
        key_list.splice(key_list.end(), key_list, (*it).second.it);
        cache_hash.insert((*it));
    }
    
    cache_hash_itr front(void) {
       return cache_hash.find(key_list.front()); 
    }
    
    cache_hash_itr end(void) {
       return cache_hash.find(key_list.end()); 
    }
    
    cache_hash_itr find(const key_type& k) {
        return cache_hash.find(k);
    }
    value_type insert(const key_type& k, const value_type& v) {
        key_list_itr it = key_list.insert(key_list.end(), k);
        cache_hash.insert(make_pair(k, struct_value_type{v, false ,it}));
    }
    
    void update(const key_type& k, const value_type& v) {
        key_list_itr it = key_list.end(); 
        cache_hash_itr hit = cache_hash.find(*it);
        (*hit).second.value = v;
        (*hit).second.dirty_bit = true;
    }
    
    void dump() {
       key_list_itr it;
       
       for(it = key_list.begin(); it != key_list.end(); it++) {
           cache_hash_itr hit = cache_hash.find(*it);
           cout << (*hit).first <<"\t"<< (*hit).second.value << endl;
       }
    }
};

template <typename key_type, typename value_type> class CacheManager {
private:
    Cache<key_type, value_type> cache[MAX_CACHE_LEVEL];
    value_type (*cacheFunc)(const key_type&);
public:

    CacheManager() { }
    CacheManager(int capacity, value_type (*f)(const key_type&)): cacheFunc(f) {
        int level = 0;
        int tempCapacity = capacity;
        for (level = 0; level < MAX_CACHE_LEVEL; level++) {
            cache[level].setCapacity(tempCapacity);
            tempCapacity *= tempCapacity;
        }
    }
    
        value_type read(const key_type& k) {
        return fetch(k);
    }
    void update(const key_type& k, const value_type& v) {
        cache[0].update(k,v);
    }
    void write(const key_type& k, const value_type& v) {
        fetch(k);
        update(k,v);
    }
    value_type fetch(const key_type& k) {
        int result = CACHE_MISS;
        int level = 0;

        while (1) {
            if(level == MAX_CACHE_LEVEL) break;
            result = cache[level].fetch(k);
            if(result == CACHE_HIT) break;
            level++;
        }
        if(result == CACHE_HIT) {
            //promote the k from cache[level] to cache[0]
            cout<<"promote:"<<k<<endl;
            return promote(k, level);
        } else {
            //fetch the k from memory and insert into cache[0] 
            cout<<"insert:"<<k<<endl;
            return insert(k);
        }
    }
    
    value_type promote(key_type k, int level) {
        typename Cache<key_type, value_type>::cache_hash_itr it = cache[level].find(k);

        //demote least recently used key to higher level cache
        demote(0);
        cache[0].splice(it);
        return ((*it).second.value);
    }

    value_type insert(const key_type& k) {
        
        //demote least recently used key to higher level cache
        demote(0);
        const value_type v = (*cacheFunc)(k);
        cache[0].insert(k, v);
        return v;
    }
    
    void demote(int level) {
        typename Cache<key_type, value_type>::cache_hash_itr it = cache[level].front();

        if (level >= MAX_CACHE_LEVEL)
            return;
        if (!cache[level].isFull())
            return;
        if (level == MAX_CACHE_LEVEL - 1) {
            if ((*it).second.dirty_bit == true) {
                //write to memory
            }
            cache[level].key_list.pop_front();
            cache[level].cache_hash.erase(it);
        }
        else {
            if (cache[level + 1].isFull())
                demote(level + 1);
            cache[level + 1].splice(it);
            cache[level].cache_hash.erase(it);
        }
    }

    void dump() {
        for(int level=0; level < MAX_CACHE_LEVEL; level++) {
            cout<<"======LEVEL:"<<level<<"======"<<endl;
            cache[level].dump();
        }
    }
};

int fn(const int& i) { 
    return i*i;
} 

int main(void) {
    CacheManager<int, int> cacheMgr(2, fn);
    cacheMgr.fetch(1);
    cacheMgr.fetch(2);
    cacheMgr.fetch(3);
    cacheMgr.fetch(4);
    cacheMgr.fetch(5);
    cacheMgr.fetch(6);
    cacheMgr.fetch(7);
    cacheMgr.fetch(8);
    cacheMgr.fetch(9);
    cacheMgr.fetch(10);
    cacheMgr.fetch(1);
    cacheMgr.fetch(2);
    cacheMgr.dump();
    return 0;
}
